# README

Ce projet a été réalisé dans le cadre d'un test technique pour The Brand Collector, le but étant de faire un micro service connecté à IMDB et Google Spreadsheet.

## Installation

<ol>
  <li>Installer les dépendances à l'aide de <code>npm i</code></li>
  <li>Builder le projet à l'aide de <code>npm run build</code></li>
  <li>Lancer le projet à l'aide de <code>npm run start</code></li>
</ol>

Pour relancer automatiquement le projet à chaque build <code>npm run start:dev</code>

## Principales dépendances

<ul>
  <li>Axios</li>
  <li>Request</li>
  <li>Googleapis</li>
</ul>

## Routes

<ul>
  <li>/ : Vérifier l'état de l'API (ne nécessite pas d'authentification)</li>
  <li>/caraibes : Lister les films de la franchise "Pirates des Caraïbes"</li>
  <li>/films : Lister les films de la franchise "Fast & Furious"</li>
</ul>

## Authentification

Le fichier config.json contient toutes les informations nécessaire à la connexion à OMDB, pour simplifier l'authentification auprès de notre micro service nous reprenons la même apikey que OMDB à placer dans l'url.

### Exemple

<code>http://localhost:3000/caraibes?apikey=54ea20e6</code>

## Forces & Faiblesses

### Forces

<ul>
    <li>Fortement typé</li>
    <li>Middleware d'authentification</li>
    <li>Découpage du code en services (services)</li>
    <li>/caraibes plus rapide du fait qu'on ne calcul pas la clé "isTherePaulWalker"</li>
    <li>Évolutif</li>
</ul>

### Faiblesse

<ul>
    <li>Pas de routeur (dans l'état c'est aussi une force)</li>
    <li>Perdu dans la documentation Google je n'ai pas pu faire l'intégration en ne trouvant pas comment génerer automatiquement le fichier credentials.json</li>
</ul>

## Todo

<ul>
    <li>Ajouter des tests unitaires</li>
    <li>Créer un vrai système d'authentification</li>
    <li>Mettre en place un système de cache (Axios) pour accélerer certains traitements</li>
    <li>Finaliser l'intégration dans Google Spreadsheet</li>
    <li>Paralléliser les appels vers les fiches de films pour accélerer le calcul de la clé "isTherePaulWalker"</li>
    <li>Dockeriser afin de faciliter la MEP</li>
</ul>
