import { AxiosError } from "axios";
import express from "express";
import * as config from "./config.json";
import { OMDBConfig, OMDBService } from "./services/omdbapi";

const app = express();
const port = 3000;
const omdbConfig = config.ombdapi as OMDBConfig;

const omdbService = new OMDBService(omdbConfig);
// const spreadSheetService = new SpreadSheetService();

app.use(
  (
    req: express.Request,
    resp: express.Response,
    next: express.NextFunction
  ) => {
    if (req.query.apikey !== omdbConfig.apikey && req.path !== "/") {
      resp.status(401).json({
        status: "Unauthorized",
        statusCode: 401,
        message:
          "Vous devez fournir une clé d'API dans l'URL, ex: ?apikey=xxxxxxxx",
      });
    } else {
      next();
    }
  }
);

app.get("/", (req: express.Request, resp: express.Response) => {
  resp.send("nws-the-brand-collector up");
});

app.get("/films", (req: express.Request, resp: express.Response) => {
  omdbService
    .searchMovieByName("fast & furious", true)
    .then((res) => {
      if (res.data.Error) {
        resp.status(400).json({ erreur: "Film introuvable" });
      } else {
        resp.json(res.data.Search);
      }
    })
    .catch((err: AxiosError) => {
      resp.status(500).json({ erreur: err.message });
    });
});

app.get("/caraibes", (req: express.Request, resp: express.Response) => {
  omdbService
    .searchMovieByName("pirates of the caribbean", false)
    .then((res) => {
      if (res.data.Error) {
        resp.status(400).json({ erreur: "Film introuvable" });
      } else {
        resp.json(res.data.Search);
      }
    })
    .catch((err: AxiosError) => {
      resp.status(500).json({ erreur: err.message });
    });
});

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});
