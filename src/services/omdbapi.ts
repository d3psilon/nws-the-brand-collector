import axios, { AxiosInstance } from "axios";

export interface OMDBConfig {
  apikey: string;
  url: string;
}

export interface OMDBMovie {
  Title: string;
  Year: string;
  Rated: string;
  Released: string;
  Runtime: string;
  Genre: string;
  Director: string;
  Writer: string;
  Actors: string;
  Plot: string;
  Language: string;
  Country: string;
  Awards: string;
  Poster: string;
  Ratings: OMDBRating[];
  Metascore: string;
  imdbRating: string;
  imdbVotes: string;
  imdbID: string;
  Type: string;
  DVD: string;
  BoxOffice: string;
  Production: string;
  Website: string;
  Response: string;
  // custom fields
  isBefore2015: boolean;
  isTherePaulWalker: boolean;
}

export interface OMDBSearchResult {
  totalResults: string;
  Response: String;
  Error: String;
  Search: OMDBMovie[];
}

export interface OMDBRating {
  Source: string;
  Value: string;
}

export class OMDBService {
  private _omdbAxiosInstance: AxiosInstance;

  constructor(config: OMDBConfig) {
    this._omdbAxiosInstance = axios.create({
      baseURL: `${config.url}`,
      params: {
        apikey: config.apikey,
      },
    });

    this._omdbAxiosInstance.interceptors.request.use(
      function (config) {
        console.log(
          "EXT REQUEST",
          config.method,
          config.baseURL,
          config.url,
          config.params
        );
        return config;
      },
      function (error) {
        return Promise.reject(error);
      }
    );

    this._omdbAxiosInstance.interceptors.response.use(
      function (response) {
        console.log(
          "EXT RESPONSE",
          response.config.method,
          response.config.baseURL,
          response.config.url,
          response.config.params,
          response.status
        );
        return response;
      },
      function (error) {
        return Promise.reject(error);
      }
    );
  }

  searchMovieByName(movieName: string, isFastAndFuriousMovie: boolean) {
    return this._omdbAxiosInstance
      .get<OMDBSearchResult>("", {
        params: {
          s: movieName,
          type: "movie",
        },
      })
      .then(async (result) => {
        if (!result.data.Error) {
          const movies = result.data.Search;
          for (let index = 0; index < movies.length; index++) {
            const current = movies[index];
            if (isFastAndFuriousMovie) {
              const movie = await this.searchMovieByID(current.imdbID);
              current.isTherePaulWalker =
                movie.data.Actors.indexOf("Paul Walker") !== -1 ? true : false;
            }
            current.isBefore2015 =
              parseInt(current.Year, 10) < 2015 ? true : false;
          }
        }
        return result;
      });
  }

  searchMovieByID(imdbID: string) {
    return this._omdbAxiosInstance.get<OMDBMovie>("", {
      params: {
        i: imdbID,
        type: "movie",
      },
    });
  }
}
